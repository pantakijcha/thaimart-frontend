import Vue from "vue";
import VueRouter from "vue-router";
import LiffLoginPage from "@/views/Line/Login";
import Dashboard from "@/views/Dashboard";

Vue.use(VueRouter);

const routes = [
  {
    path: "",
    name: "LiffLogin",
    meta: {
      auth: false,
    },
    component: LiffLoginPage,
  },
  {
    path: "/dashboard",
    component: Dashboard,
    children: [
      {
        name: "ReferenceHistory",
        path: "/",
        meta: {
          auth: true,
        },
        // lazy-loaded
        component: () => import("@/views/Dashboard/thaimart/ReferenceHistory"),
      },
      {
        name: "ManageEmployee",
        path: "manage-employee",
        meta: {
          auth: true,
        },
        // lazy-loaded
        component: () => import("@/views/Dashboard/thaimart/ManageEmployee"),
      },
    ],
  },
  {
    path: "/realtime-monitor",
    name: "RealTimeMonitor",
    meta: {
      auth: false,
    },
    component: () => import("@/views/RealTimeMonitor"),
  },
  {
    path: "/403",
    name: "403",
    meta: {
      auth: false,
    },
    component: () => import("@/views/403"),
  },
  {
    path: "*",
    name: "404",
    meta: {
      auth: false,
    },
    component: () => import("@/views/404"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: routes,
});

export default router;
