import axios from "axios";
import authHeader from "@/services/auth/header";

class EmployeeService {
  async GetAllEmployee() {
    let res = await axios.get(`/staff`, { headers: authHeader() });
    return res.data;
  }

  async NewEmployee(employeeData) {
    let res = await axios.post(`/staff`, employeeData, {
      headers: authHeader(),
    });
    return res.data;
  }

  async InActiveEmployee(employeeData) {
    let res = await axios.delete(`/staff/${employeeData.id}`, {
      headers: authHeader(),
    });
    return res.data;
  }
}

export default new EmployeeService();
