import axios from "axios";

class LineAuthService {
  async signIn(liffToken) {
    let res = await axios.post(`/auth/verify`, {
      token: liffToken,
    });

    return res.data;
  }
}

export default new LineAuthService();
