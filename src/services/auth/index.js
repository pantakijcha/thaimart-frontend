import axios from "axios";
import authHeader from "@/services/auth/header";

class AuthService {
  async signOut() {
    try {
      await axios.post(`/auth/logout`, {}, { headers: authHeader() });
    } catch (err) {
      console.log(`Sign out error : ${err}`);
    }
  }

  async refreshToken() {
    let res = await axios.get(`/auth/refresh`, { headers: authHeader() });

    return res.data;
  }
}

export default new AuthService();
