import axios from "axios";
import authHeader from "@/services/auth/header";

class CustomerService {
  async AddFriendWithStaff(data) {
    let res = await axios.post(`/customers`, data);
    return res.data;
  }

  async GetSummaryReport(month, year) {
    let citeria = "";
    if (month > 0) {
      let monthCiteria = `month=${month}`;
      citeria = citeria.includes("?")
        ? `${citeria}&${monthCiteria}`
        : `?${monthCiteria}`;
    }

    if (year > 0) {
      let yearCiteria = `year=${year}`;
      citeria = citeria.includes("?")
        ? `${citeria}&${yearCiteria}`
        : `?${yearCiteria}`;
    }

    let res = await axios.get(`/customers/summary${citeria}`, {
      headers: authHeader(),
    });
    return res.data;
  }

  async GetSummaryCurrentMonth(customerLimit, staffLimit) {
    let res = await axios.get(
      `/customers/realtime-summary?customerLimnit=${customerLimit}&staffLimit=${staffLimit}`,
      {
        headers: authHeader(),
      }
    );
    return res.data;
  }
}

export default new CustomerService();
