import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import router from "@/router";
import VueAxios from "vue-axios";
import axios from "axios";
import store from "@/store";
import VueMobileDetection from "vue-mobile-detection";
import { appSetting } from "@/setup";
import VueFullscreen from "vue-fullscreen";

Vue.config.productionTip = false;

Vue.use(VueAxios, axios);
Vue.use(VueMobileDetection);
Vue.use(VueFullscreen);

Vue.axios.defaults.baseURL = appSetting.apiUrl;

// Route guard.
router.beforeEach((to, from, next) => {
  let isAuthenticate = store.state.auth.isAuthenticated;
  if (to.meta.auth && !isAuthenticate) {
    next({ name: "LiffLogin" });
  } else {
    next();
  }
});

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
