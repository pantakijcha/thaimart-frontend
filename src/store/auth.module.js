import LineAuthService from "@/services/auth/line";
import AuthService from "@/services/auth/";
// import jwt_decode from "jwt-decode";

let appToken = localStorage.getItem("app_token");

const initialState = {
  isAuthenticated: false,
  appToken,
  refreshInterval: null,
  // lineId: null,
  // lineName: null,
};

export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    async lineAuth({ commit, dispatch }, liffToken) {
      try {
        let res = await LineAuthService.signIn(liffToken);
        commit("setLineData", res.token);
      } catch (err) {
        dispatch("signOut");
        throw err;
      }
    },
    async signOut({ commit }) {
      await AuthService.signOut();
      commit("signOut");
    },
    async setRefreshTokenInterval({ commit }) {
      let refresToken = async function() {
        try {
          let res = await AuthService.refreshToken();
          commit("refreshTokenSuccess", res.refresh_token);
        } catch (err) {
          commit("signOut");
          throw err;
        }
      };

      await refresToken();
      let interval = setInterval(
        refresToken,
        30 * 60000 // 60000 milliseconds = 1 minute
      );

      commit("setRefreshTokenInterval", interval);
    },
  },
  mutations: {
    signOut(state) {
      state.isAuthenticated = false;
      state.appToken = null;
      clearInterval(state.refreshInterval);

      localStorage.clear();
    },
    refreshTokenSuccess(state, token) {
      state.isAuthenticated = true;
      state.appToken = token;
      localStorage.setItem("app_token", token);
    },
    setRefreshTokenInterval(state, interval) {
      state.refreshInterval = interval;
    },
    setLineData(state, token) {
      state.appToken = token;
      // let tokenData = jwt_decode(token);
      state.isAuthenticated = true;
      // state.lineName = tokenData.lineName;
      // state.lineId = tokenData.lineId;
    },
  },
};
