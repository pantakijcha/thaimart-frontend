import Vue from "vue";
import Vuex from "vuex";
import { auth } from "@/store/auth.module";

const initialState = {
  globalLoading: false,
};

Vue.use(Vuex);

export default new Vuex.Store({
  state: initialState,
  actions: {
    ApplicationIsLoading({ commit }, isLoading) {
      commit("SetLoadingState", isLoading);
    },
  },
  mutations: {
    SetLoadingState(state, isLoading) {
      state.globalLoading = isLoading;
    },
  },
  modules: { auth },
});
